import unittest
import volume_calculator


class Test_Volume_Calculator(unittest.TestCase):
    def test_cube_calculator(self):
        result = volume_calculator.compute_cube(1, 2, 3)
        self.assertEqual(result, 6)

        result = volume_calculator.compute_cube(3, 3, 3)
        self.assertEqual(result, 27)

        result = volume_calculator.compute_cube(1, 1, 1)
        self.assertEqual(result, 1)

        # Expected to fail
        result = volume_calculator.compute_cube(0, 0, 0)
        self.assertEqual(result, 1)


if __name__ == '__main__':
    unittest.main()
